﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace RESTEmployee.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData {
            [Key]
            public int Empid;
            [Required(ErrorMessage ="Ingresa un nombre")]
            public string Empname;
            [Required]
            [EmailAddress(ErrorMessage = "Ingresa un correo")]
            public string Email;
            [Required(ErrorMessage = "rango de edad excedido")]
            public Nullable<int> Age;
            [Required(ErrorMessage = "rango de edad excedido")]
            public Nullable<int> Salary;
        }

    }
}