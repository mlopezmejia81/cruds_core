﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Empleados.Models
{
    public class EmployeeSearchViewModel
    {

        public List<Employee> Employees { get; set; }
        public string Empname { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public int Salary { get; set; }


    }
}
