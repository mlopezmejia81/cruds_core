﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Empleados.Models
{
    //para que funcione tenemos que abrir tools-package nuget managment- manage packages for solution
    //en el buscador buscamos Entityframework y seleccionamos el microsoft.entityframeworkcore.sqlserver y seleccionamos las casillas de proyecto y de Core
    //y lo instalammos
    public class ApplicationDbContext :DbContext//herada de DbContext
        //cuando nos marque error podemos ver las posibles soluciones seleccionando el problema
    {
        //constructor
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        //entidades que se guardan <Entity>
        public DbSet<Employee> Employee { get; set; }
    }
}
