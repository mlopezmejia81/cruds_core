﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Empleados.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Empleados.Controllers
{
    public class EmployeeController : Controller
    {
        //esto es como inyeccion de dependencias
        private readonly ApplicationDbContext _db;//variable para acceder a la base de datos, una vez que sea creada en el constructor nunca mas se pondra cambiar

        //constructor
        public EmployeeController(ApplicationDbContext db)
        {
            _db = db;
        }

        //nos permite retornar una vista con el IActionResult
      
        public IActionResult Create()//cuando estamos en la primera vista y damos clic en create se va a esta vista, pero cuando ingresamos 
                                     //el nuevo empleado entonces se ira a el metodo POST que esta a continuacion
        {
            return View();
        }


        //cuando le demos clic en create view en el browswer y llenemos el formulario se ira a esta funcion 
        //es una atrea asincorna, no hara nada hasta que se haga bien la tarea
        [HttpPost]
        public async Task<ActionResult> Create(Employee nEmp)
        {
            //si el modelo es valido 
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }

        //generamos el metodo para poder acceder a la opcion de edit en el index
        public async Task<ActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getEmp = await _db.Employee.FindAsync(id);
            return View(getEmp);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getEmp = await _db.Employee.FindAsync(id);
            return View(getEmp);
        }

        //task es para una consulta asincrona
        [HttpPost]
        public async Task<ActionResult> Edit(Employee oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(oldEmp);
        }

        public async Task<ActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getEmp = await _db.Employee.FindAsync(id);
            return View(getEmp);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getEmpdetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpdetail);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");


        }
        public async Task<IActionResult> Index(string searchString)
        {


            var displaydata = _db.Employee.ToList();

            var getEmpdetail = from m in _db.Employee
                         select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                getEmpdetail = getEmpdetail.Where(s => s.Empname.Contains(searchString));
            }
            

            return View( await getEmpdetail.ToListAsync());
        }
    }
}
