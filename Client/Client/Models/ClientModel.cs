﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Models
{
    public class ClientModel
    {
        [Key]
        public int client_id { get; set; }

        [Required(ErrorMessage = "Enter the client name")]
        [Display(Name = "Client Name")]
        public string client_name { get; set; }

        [Required(ErrorMessage = "Enter if the client has a credit ")]
        [Display(Name = "Credit")]
        public string client_credit { get; set; }

        [Required(ErrorMessage = "Enter the client age")]
        [Display(Name = "Age")]
        [Range(20, 50)]
        public int client_age { get; set; }
  
        [Required(ErrorMessage = "Enter the client email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Enter a valid email address")]
        public string client_email { get; set; }

    }
}
