﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Client.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Client.Controllers
{
    public class ClientController : Controller
    {


        private readonly ApplicationDbContext _db;

        public ClientController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index() //En INDEX haremos que liste a todos los empleados en la BD (READ)
        {
            var displaydata = _db.ClientDb.ToList();
            return View(displaydata);
        }
        [HttpGet]
        public async Task<IActionResult> Index(string searchString)
        {


           // var displaydata = _db.ClientDb.ToList();// es _db.nombre de la tabla

            var getClientdetail = from m in _db.ClientDb
                                  select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                getClientdetail = getClientdetail.Where(s => s.client_name.Contains(searchString));
            }


            return View(await getClientdetail.ToListAsync());
        }

        public async Task<ActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getClient = await _db.ClientDb.FindAsync(id);
            return View(getClient);
        }

        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Create(ClientModel nuevo) //se le pasa algo del tipo del modelo!!!!!!!!!!!
        {
            if (ModelState.IsValid)
            {
                _db.Add(nuevo);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }

            return View(nuevo);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getClient = await _db.ClientDb.FindAsync(id);
            return View(getClient);
        }

        //task es para una consulta asincrona
        [HttpPost]
        public async Task<ActionResult> Edit(ClientModel oldClient)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldClient);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(oldClient);
        }

        public async Task<ActionResult> Delete(int? id)
        {

            if (id == null)
            {
                return RedirectToAction("Index");

            }
            var getClient = await _db.ClientDb.FindAsync(id);
            return View(getClient);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getClient = await _db.ClientDb.FindAsync(id);
            _db.ClientDb.Remove(getClient);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");


        }
    }
}
