//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RESTClient.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientDb
    {
        public int client_id { get; set; }
        public string client_name { get; set; }
        public string client_credit { get; set; }
        public Nullable<int> client_age { get; set; }
        public string client_email { get; set; }
    }
}
