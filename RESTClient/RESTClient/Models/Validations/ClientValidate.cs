﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RESTClient.Models
{
    [MetadataType(typeof(ClientDb.MetaData))]
    public partial class ClientDb
    {
        
        sealed class MetaData
        {
            [Key]
            public int client_id;


            [Required(ErrorMessage = "Ingresa un nombre")]
            public string client_name;


            [Required(ErrorMessage = "Ingresa si el cliente tiene un credito")]
            public string client_credit;


            [Required(ErrorMessage = "rango de edad excedido")]
            public Nullable<int> client_age;

            [Required]
            [EmailAddress(ErrorMessage = "ingrese un email correcto")]
            public string client_email;

        }
    }
}