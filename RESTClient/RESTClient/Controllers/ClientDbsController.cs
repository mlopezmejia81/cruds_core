﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RESTClient.Models;

namespace RESTClient.Controllers
{
    public class ClientDbsController : ApiController
    {
        private ClienteEntities db = new ClienteEntities();

        // GET: api/ClientDbs
        public IQueryable<ClientDb> GetClientDbs()
        {
            return db.ClientDbs;
        }

        // GET: api/ClientDbs/5
        [ResponseType(typeof(ClientDb))]
        public IHttpActionResult GetClientDb(int id)
        {
            ClientDb clientDb = db.ClientDbs.Find(id);
            if (clientDb == null)
            {
                return NotFound();
            }

            return Ok(clientDb);
        }

        // PUT: api/ClientDbs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClientDb(int id, ClientDb clientDb)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != clientDb.client_id)
            {
                return BadRequest();
            }

            db.Entry(clientDb).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientDbExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ClientDbs
        [ResponseType(typeof(ClientDb))]
        public IHttpActionResult PostClientDb(ClientDb clientDb)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ClientDbs.Add(clientDb);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = clientDb.client_id }, clientDb);
        }

        // DELETE: api/ClientDbs/5
        [ResponseType(typeof(ClientDb))]
        public IHttpActionResult DeleteClientDb(int id)
        {
            ClientDb clientDb = db.ClientDbs.Find(id);
            if (clientDb == null)
            {
                return NotFound();
            }

            db.ClientDbs.Remove(clientDb);
            db.SaveChanges();

            return Ok(clientDb);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClientDbExists(int id)
        {
            return db.ClientDbs.Count(e => e.client_id == id) > 0;
        }
    }
}